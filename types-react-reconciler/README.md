# Installation
> `npm install --save @types/react-reconciler`

# Summary
This package contains type definitions for react-reconciler (https://reactjs.org/).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/react-reconciler.

### Additional Details
 * Last updated: Tue, 07 Nov 2023 09:09:39 GMT
 * Dependencies: [@types/react](https://npmjs.com/package/@types/react)

# Credits
These definitions were written by [Nathan Bierema](https://github.com/Methuselah96), [Zhang Haocong](https://github.com/zhanghaocong), and [Mathieu Dutour](https://github.com/mathieudutour).
