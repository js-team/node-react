# Installation
> `npm install --save @types/react-is`

# Summary
This package contains type definitions for react-is (https://reactjs.org/).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/react-is.

### Additional Details
 * Last updated: Thu, 25 Apr 2024 20:07:03 GMT
 * Dependencies: [@types/react](https://npmjs.com/package/@types/react)

# Credits
These definitions were written by [Avi Vahl](https://github.com/AviVahl), [Christian Chown](https://github.com/christianchown), and [Sebastian Silbermann](https://github.com/eps1lon).
